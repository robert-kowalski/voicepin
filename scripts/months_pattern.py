"""
    Regex pattern covering polish words of months. Can be
    use for recongnition months

    Variables:
    MONTHS_PATTERN - contains all months pattern
    month_data - dictionary contains data about months, uses to convert
                words of months to numbers e.g.
                ...
                    'm_january': {'number': 1, 'name': 'january'},
                ...
    Function:
    get_number_month - return number according to given group of months


"""
from polish_numbers import numbers

MONTHS_PATTERN = r'''
        (?P<month>
            (?P<m_december>                                       # December
                (grud) (zień | nia) |
                {twelve}
            ) |
            (?P<m_november>                                       # November
                (listopad)(a)? |
                {eleven}
            ) |
            (?P<m_october>                                        # October
                (październik)(a)? |
                {ten}
            ) |
            (?P<m_september>                                      # September
                (wrze)(śnia | sień) |
                ((zero) \s)? {nine}
            ) |
            (?P<m_august>                                         # August
                (sierp)(ień | nia) |
                ((zero) \s)? {eight}
            ) |
            (?P<m_july>                                           # July
                (lip)(iec | ca) |
                ((zero) \s)? {seven}
            ) |
            (?P<m_june>                                           # June
                (czerw)(iec | ca) |
                ((zero) \s)? {six}
            ) |
            (?P<m_may>                                            # May
                (maj)(a)? |
                ((zero) \s)? {five}
            ) |
            (?P<m_april>                                          # April
                (kwie)(cień|tnia) |
                ((zero) \s)? {four}
            ) |
            (?P<m_march>                                          # March
                (mar)(zec|ca) |
                ((zero) \s)? {three}
            ) |
            (?P<m_february>                                       # February
                (lut)(y|ego) |
                ((zero) \s)? {two}
            ) |
            (?P<m_january>                                        # January
                (stycz)(nia|eń) |
                ((zero) \s)? {one}                                #mayby(zero)?
            )
        )
     '''.format(**numbers)

month_data = {
    'm_january': {'number': 1, 'name': 'january'},
    'm_february': {'number': 2, 'name': 'february'},
    'm_march': {'number': 3, 'name': 'march'},
    'm_april': {'number': 4, 'name': 'april'},
    'm_may': {'number': 5, 'name': 'may'},
    'm_june': {'number': 6, 'name': 'june'},
    'm_july': {'number': 7, 'name': 'july'},
    'm_august': {'number': 8, 'name': 'august'},
    'm_september': {'number': 9, 'name': 'september'},
    'm_october': {'number': 10, 'name': 'october'},
    'm_november': {'number': 11, 'name': 'november'},
    'm_december': {'number': 12, 'name': 'december'}
}


def get_number_month(group_name):
    if group_name in month_data.keys():
        return month_data[group_name]['number']
    else:
        return None
