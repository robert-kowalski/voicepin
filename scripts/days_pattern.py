"""
    Regex pattern covering polish words of numbers from 1 to 39. Can be use 
    for recongnition day in month

    Variables:
    DAYS_1_19_PATTERN - contains numbers pattern 1-19
    DAYS_TENS_PATTERN - contains tens numbers pattern 20 - 30
    numbers_data - dictionary to convert alphanumeric number (words) to 
                    number (int) 
    Function:
    get_number_day - return number according to given group

"""

from polish_numbers import numbers

DAYS_1_19_PATTERN = r'''
    (?P<day>
        (?P<d_1>
            ((zero) \s)? {one}
        ) |
        (?P<d_2>
            ((zero) \s)? {two}
        ) |
        (?P<d_3>
            ((zero) \s)? {three}
        ) |
        (?P<d_4>
            ((zero) \s)? {four}
        ) |
        (?P<d_5>
            ((zero) \s)? {five}
        ) |
        (?P<d_6>
            ((zero) \s)? {six}
        ) |
        (?P<d_7>
            ((zero) \s)? {seven}
        ) |
        (?P<d_8>
            ((zero) \s)? {eight}
        ) |
        (?P<d_9>
            ((zero) \s)? {nine}
        ) |
        (?P<d_10>
            {ten}
        ) |
        (?P<d_11>
            {eleven}
        ) |
        (?P<d_12>
            {twelve}
        )
         |
        (?P<d_13>
            {thirteen}
        )
         |
        (?P<d_14>
            {fourteen}
        )
         |
        (?P<d_15>
            {fifteen}
        )
         |
        (?P<d_16>
            {sixteen}
        ) |
        (?P<d_17>
            {seventeen}
        ) |
        (?P<d_18>
            {eighteen}
        ) |
        (?P<d_19>
            {nineteen}
        )
    )
    '''.format(**numbers)

DAYS_TENS_PATTERN = r'''
    (?P<tens>
        (?P<d_20>
            {twenty}
        ) |
        (?P<d_30>
            {thirty}
        )
    )
    '''.format(**numbers)


numbers_data = {
    'd_1': 1,
    'd_2': 2,
    'd_3': 3,
    'd_4': 4,
    'd_5': 5,
    'd_6': 6,
    'd_7': 7,
    'd_8': 8,
    'd_9': 9,
    'd_10': 10,
    'd_11': 11,
    'd_12': 12,
    'd_13': 13,
    'd_14': 14,
    'd_15': 15,
    'd_16': 16,
    'd_17': 17,
    'd_18': 18,
    'd_19': 19,
    'd_20': 20,
    'd_30': 30,
}


def get_number_day(group_number):
    """
         Return number according to given group
         :params group_number: group name of number
         return number(int)
    """
    if type(group_number) is list:
        number = 0
        for group in group_number:
            number += numbers_data[group]
        return number
    if group_number in days_data.keys():
        return numbers_data[group_number]
    else:
        print(group_number)
        return None
