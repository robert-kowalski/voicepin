"""
Checks the correctness of the translation of a text date to numbers.
I accept an appropriate csv file at the input, use a check function that
returns a dictionary in the format:

dict({
        'day': number,
        'month':number,
        'year': number
    })
"""

import pandas as pd
import argparse

from date_recognitor import date_recognition


class Checker:
    """
        Checks the correctness of the translation of a text to the pattern
        delivered by special function.

    """

    def __init__(self, checking_function):
        """
            Initial function to set parameters:
            :params checking_function: a special function to check correctness,
                have to return dict({'day': number,
                                    'month': number,
                                    'year': number})
        """
        self.columns_name = None
        self.good_matches = 0
        self.good_matches_overall = 0
        self.good_columns_matches = dict()
        self.data = None
        self.checking_function = checking_function

    def load_data(self, csv_file_path=None):
        """
            Load data to dataframe from csv file.
            "params csv_file_path: path to csv file contains data
        """
        try:
            self.data = pd.read_csv(csv_file_path)
            self.columns_name = tuple(self.data.columns.values)
            self.number_of_rows = self.data.shape[0]
            for name in self.columns_name[:-1]:
                self.good_columns_matches[name] = 0
            # result['overall_checked'] = number_of_rows

        except Exception as e:
            print(e)

    def check(self, print_differences=False):
        """
            Check correctness data according to check function
            :params print_differences: boolean, if is true print differences
            between rigth data and recongnition data, default False
        """
        if self.data.empty:
            print('After check please use load_data(csv_file) to load data')
        for index, row in self.data.iterrows():
            # date_recognition(row['text'])
            right_date = dict({
                'day': row[self.columns_name[0]],
                'month': row[self.columns_name[1]],
                'year': row[self.columns_name[2]]
            })
            checked_date = self.checking_function(row[self.columns_name[3]])
            good_comparison = 0
            for key, value in right_date.items():
                if right_date[key] == checked_date[key]:
                    self.good_columns_matches[key] += 1
                    good_comparison += 1

            self.good_matches_overall += good_comparison

            if right_date == checked_date:
                self.good_matches += 1
            elif print_differences:
                print(str(checked_date) + ' Right date: ' + str(right_date))

    def get_result(self,):
        """
            Return a result dict of computation
        """
        result = dict({
            'overall_checked': self.number_of_rows,
            'good_matches': self.good_matches,
            'good_matches_overall': self.good_matches_overall,
            'good_day': self.good_columns_matches[self.columns_name[0]],
            'good_month': self.good_columns_matches[self.columns_name[1]],
            'good_year': self.good_columns_matches[self.columns_name[2]]
        })
        return result


if __name__ == '__main__':

    # Initial values
    csv_file_path = None

    # Script arguments
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument(
        'csv', help='string containing path to csv file', type=str)

    args = parser.parse_args()

    if args.csv:
        csv_file_path = args.csv

    #  Use Checker class
    checker = Checker(checking_function=date_recognition)
    checker.load_data(csv_file_path=csv_file_path)
    checker.check(print_differences=False)

    # print result
    for key, value in checker.get_result().items():
        print(key + ': ' + str(value))
