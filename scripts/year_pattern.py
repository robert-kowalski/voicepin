"""
    Regex pattern covering polish words of numbers from 1 to 9999. Can be
    use for recongnition years

    Variables:
    YEAR_1_19_PATTERN - contains numbers pattern 1-19
    YEAR_TENS_PATTERN - contains tens numbers pattern 20 - 90
    YEAR_HUNDREDS_PATTERN - contains hundreds numbers pattern 100 - 900
    YEAR_THOUSANDS_PATTERN - contains thousands numbers pattern 1000 - 9000

    numbers_data - dictionary to convert alphanumeric number (words) to
                    number (int)
    Function:
    get_number_year - return number according to given group


"""
import operator
from polish_numbers import numbers

YEAR_1_19_PATTERN = r'''
    (#?P<year>
            (?P<y_10>
            {ten}
        ) |
        (?P<y_11>
            {eleven}
        ) |
        (?P<y_12>
            {twelve}
        )
         |
        (?P<y_13>
            {thirteen}
        )
         |
        (?P<y_14>
            {fourteen}
        )
         |
        (?P<y_15>
            {fifteen}
        )
         |
        (?P<y_16>
            {sixteen}
        ) |
        (?P<y_17>
            {seventeen}
        ) |
        (?P<y_18>
            {eighteen}
        ) |
        (?P<y_19>
            {nineteen}
        ) |
        (?P<y_1>
            ((zero) \s)? {one}
        ) |
        (?P<y_2>
            ((zero) \s)? {two}
        ) |
        (?P<y_3>
            ((zero) \s)? {three}
        ) |
        (?P<y_4>
            ((zero) \s)? {four}
        ) |
        (?P<y_5>
            ((zero) \s)? {five}
        ) |
        (?P<y_6>
            ((zero) \s)? {six}
        ) |
        (?P<y_7>
            ((zero) \s)? {seven}
        ) |
        (?P<y_8>
            ((zero) \s)? {eight}
        ) |
        (?P<y_9>
            ((zero) \s)? {nine}
        )
    )
    '''.format(**numbers)

YEAR_TENS_PATTERN = r'''
    (#?P<y_tens>
        (?P<y_20>
            {twenty}
        ) |
        (?P<y_30>
            {thirty}
        ) |
        (?P<y_40>
            {fourty}
        ) |
        (?P<y_50>
            {fifty}
        ) |
        (?P<y_60>
            {sixty}
        ) |
        (?P<y_70>
            {seventy}
        ) |
        (?P<y_80>
            {eighty}
        ) |
        (?P<y_90>
            {ninety}
        )
    )
    '''.format(**numbers)

YEAR_HUNDREDS_PATTERN = r'''
    (#?P<y_hundreds>
        (?P<y_100>
            {hundred}
        ) |
        (?P<y_200>
            {two_hundred}
        ) |
        (?P<y_300>
            {three_hundred}
        ) |
        (?P<y_400>
            {four_hundred}
        ) |
        (?P<y_500>
            {five_hundred}
        ) |
        (?P<y_600>
            {six_hundred}
        ) |
        (?P<y_700>
            {seven_hundred}
        ) |
        (?P<y_800>
            {eight_hundred}
        ) |
        (?P<y_900>
            {nine_hundred}
        )
    )
    '''.format(**numbers)

YEAR_THOUSANDS_PATTERN = r'''
    (#?P<y_hundreds>
        (?P<y_1000>
            {thousand}
        ) |
        (?P<y_2000>
            {two_thousand}
        )|
        (?P<y_3000>
            {three_thousand}
        )|
        (?P<y_4000>
            {four_thousand}
        )|
        (?P<y_5000>
            {five_thousand}
        )|
        (?P<y_6000>
            {six_thousand}
        )|
        (?P<y_7000>
            {seven_thousand}
        )|
        (?P<y_8000>
            {eight_thousand}
        )|
        (?P<y_9000>
            {nine_thousand}
        )
    )
    '''.format(**numbers)

numbers_data = {
    'y_1': 1,
    'y_2': 2,
    'y_3': 3,
    'y_4': 4,
    'y_5': 5,
    'y_6': 6,
    'y_7': 7,
    'y_8': 8,
    'y_9': 9,
    'y_10': 10,
    'y_11': 11,
    'y_12': 12,
    'y_13': 13,
    'y_14': 14,
    'y_15': 15,
    'y_16': 16,
    'y_17': 17,
    'y_18': 18,
    'y_19': 19,
    'y_20': 20,
    'y_30': 30,
    'y_40': 40,
    'y_50': 50,
    'y_60': 60,
    'y_70': 70,
    'y_80': 80,
    'y_90': 90,
    'y_100': 100,
    'y_200': 200,
    'y_300': 300,
    'y_400': 400,
    'y_500': 500,
    'y_600': 600,
    'y_700': 700,
    'y_800': 800,
    'y_900': 900,
    'y_1000': 1000,
    'y_2000': 2000,
    'y_3000': 3000,
    'y_4000': 4000,
    'y_5000': 5000,
    'y_6000': 6000,
    'y_7000': 7000,
    'y_8000': 8000,
    'y_9000': 9000,
}


def get_number_year(group_number):
    """
         Return number according to given group
         :params group_number: group name of number
         return number(int)
    """
    sorted_number = sorted(group_number)
    number = 0
    multiply = 1
    if len(group_number) == 1:
        last_index = sorted_number[0]
        return 1900 + numbers_data[group_number[last_index]]
    if len(group_number) == 2:
        tens = False
        leng = len(''.join(group_number.values()))
        if leng == 8:
            for i in sorted_number[::-1]:
                if tens:
                    multiply *= 100
                number += numbers_data[group_number[i]] * multiply
                tens = True
            return number
        else:
            thousand = False
            for i in sorted_number[::-1]:

                if len(group_number[i]) == 4 and tens:
                    tens = False
                if len(group_number[i]) == 6 and tens:
                    tens = False
                    thousand = True
                if tens:
                    multiply *= 10
                number += numbers_data[group_number[i]] * multiply
                tens = True
            if thousand:
                return number
            return number + 1900
    else:
        tens = True
        last_index = sorted_number[0]
        if len(group_number[last_index]) != 4:
            tens = False
        for i in sorted_number[::-1]:
            if last_index == i and tens:
                multiply *= 100
            number += numbers_data[group_number[i]] * multiply
        return number
    return None
