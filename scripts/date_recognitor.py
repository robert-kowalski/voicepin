"""
    Module deliver a function to recognition polish text with date.
    To use: import mai function.
    To see how it work run module in shell.
    sample_text: już podaje trzeci zero czwarty dziewiętnaście
                 czterdzieści pięć

    Main function:
    date_recognition - accepts text as parameter and return data as disctionary
                        e.q
                        {
                            'day': 3,
                            'month': 4,
                            'year': 1945
                        }
    Use modules:
    days_pattern.py - to recognition days
    months_pattern.py - to recognition months
    year_pattern.py - to recognition years

    Indirectly use:
    polish_numbers.py
"""

import re
from months_pattern import MONTHS_PATTERN, get_number_month
from days_pattern import DAYS_1_19_PATTERN, DAYS_TENS_PATTERN, \
    get_number_day
from year_pattern import YEAR_1_19_PATTERN, YEAR_TENS_PATTERN,  \
    YEAR_HUNDREDS_PATTERN, get_number_year, YEAR_THOUSANDS_PATTERN


def date_recognition(text):
    """
        Function to recognition polish text with dates and convert to
        dictionary e.g:
                        {
                            'day': 1,
                            'month': 5,
                            'year': 1925
                        }
        :params text: polish text with date (words, no numbers)
        :return dict: dictionary with numbers of diagnosed numbers
    """

    PATTERN = r'(\w* | \s*)' +                              \
        r'(' + f'{DAYS_TENS_PATTERN}' + r'  \s )*' +        \
        r'(' + f'{DAYS_1_19_PATTERN}' + r')+?' + r'\s' +    \
        f'{MONTHS_PATTERN}' + r'\s*' +                      \
        r'(' + f'{YEAR_THOUSANDS_PATTERN}' + r'\s* )*' +    \
        r'(' + f'{YEAR_HUNDREDS_PATTERN}' + r'\s* )*' +     \
        r'(' +                                              \
        r'(' + f'{YEAR_TENS_PATTERN}' + r'\s* )|' +         \
        r'(' + f'{YEAR_1_19_PATTERN}' + r'\s* )'            \
        r')*' + r'\s*'

    dataPattern = re.compile(PATTERN, re.VERBOSE)
    match = re.search(dataPattern, text)

    days = list()
    month_string = ''
    year_numbers = dict()

    if match:
        dict_match = match.groupdict()
        match_text = match.string
        match_text = match_text.split(' ')
        for key, value in dict_match.items():
            if value and key.startswith('d_'):
                days.append(key)
                [match_text.remove(word) for word in value.split(' ')]
            elif value and key.startswith('m_'):
                month_string = key
                [match_text.remove(word) for word in value.split(' ')]

            elif value and key.startswith('y_'):
                is_on_list = False
                for word in value.split(' '):
                    index = match_text.index(word)
                    match_text[index] = ''
                    if not is_on_list:
                        year_numbers[index] = key
                        is_on_list = True
                        if len(key[2:]) == 1:
                            if word in match_text:
                                index = match_text.index(word)
                                match_text[index] = ''
                                year_numbers[index+1] = key
    return dict({
        'day': get_number_day(days),
        'month': get_number_month(month_string),
        'year': get_number_year(year_numbers)
    })


if __name__ == '__main__':
    sample = 'już podaje trzeci zero czwarty dziewiętnaście czterdzieści pięć'
    print(date_recognition(sample))
