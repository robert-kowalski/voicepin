## Table of contents
1. [Repository description](#Date_recognitor)
2. [How to use](#how_to_use)
3. [Architecture](#architecture)
4. [Copyright](#copyright)

## Date recognitor

Simple example of using Python build-in library to regex.
The module recognizes Polish dates written in words and returns converted dates in the form of a dictionary with numbers.

e.g

Input:
```
"już podaje trzeci zero czwarty dziewiętnaście
 czterdzieści pięć"
```
Output:
```python
{
    'day': 3,
    'month': 4,
    'year': 1945
}
```

The solution was written in [Python 3.7.0](https://www.python.org/downloads/release/python-370/).

Used libraries:
- [pandas](https://pandas.pydata.org/)
- [re](https://docs.python.org/3/library/re.html)

## How to use

Local machine:
1. Clone repository 
  ``` 
  git clone https://gitlab.com/robert-kowalski/voicepin.git
  ```
2. Install the required libraries using e.g. 
  ```
  pip install -r .\requirements.txt 
  ```
3. To see an example of an activity, go to main directiory    and run:
```
python scripts/date_recongnitor.py
```  
4. For normal use, simply import the ```date_recognition```    functions from the ```date_recognition.py``` file

```python
from scripts.date_recognitor import date_recognition
```
5. To check the larger amount of data stored in the csv        file in the predicted form use: 
```
python3 scripts/checker.py <path_to_csv_file>
```
E.g:
```
python3 scripts/checker.py ./data/test_data.csv
```

Docker:
1. Install [Docker](https://www.docker.com/)
2. Clone repository
```
git clone https://gitlab.com/robert-kowalski/voicepin.git
```
3. Enter to main folder and run 
```
docker-compose up -d --build
```
4. Check name of created container by run 
```
docker ps
```
Should be somethink like
```
voicepin_app_1
```
5. To enter into container type:
```
docker exec -it voicepin_app_1 /bin/bash
```
In another terminal window:
1. To see an example of an activity, go to main directiory    and run:
```
python3 scripts/date_recongnitor.py
```  
2. For normal use, simply import the ```date_recognition```    functions from the ```date_recognition.py``` file
```python
from scripts.date_recognitor import date_recognition
```
3. To check the larger amount of data stored in the csv        file in the predicted form use: 
```
python3 scripts/checker.py <path_to_csv_file>
```
E.g:
```
python3 scripts/checker.py ./data/test_data.csv
```

##### Protip

All modules in the scripts folder are required.

## Architecture
- ***scripts*** - contains python files
- ***data*** - inside are some example of test dates

Required format:
```
day,month,year,text
3,4,1945,już podaje trzeci zero czwarty dziewiętnaście czterdzieści pięć
3,2,2091,to będzie trzeciego lutego dwadzieścia dziewięćdziesiąt jeden
...
```
- ***scripts/checker.py*** - script to checkig correcness of conversion
- ***scripts/date_recognitor.py*** - module with recognition function
- ***scripts/days_pattern.py*** - Regex pattern     covering polish words of numbers from 1 to 39. Can be use 
for recongnition day in month
- ***scripts/months_pattern.py*** - Regex pattern covering polish words of months. Can be use for recongnition months
- ***scripts/year_pattern.py*** - Regex pattern covering polish words of numbers from 1 to 9999. Can be use for recongnition years
- ***scripts/polish_numbers.py*** - Module contains one dictionary (numbers) of polish numbers pattern

## Copyright

All rights reserved.