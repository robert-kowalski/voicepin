FROM python:3.7.0-stretch

RUN mkdir /usr/src/app
WORKDIR usr/src/app

COPY ./requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ENV PYTHONUNBUFFERED 1

COPY . .

CMD tail -f /dev/null